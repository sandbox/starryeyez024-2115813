<?php

/**
 * @file
 * Template overrides as well as (pre-)process and alter hooks for the
 * panelitist theme.
 */

//function panelitist_image_style($variables) {
//  // Determine the dimensions of the styled image.
//  $image_uri = $node->field_image['und'][0]['uri'];
//  $image_for_sizing = image_style_path('blog_top', $image_uri);
//  $image_dimentions = getimagesize($image_for_sizing);
//  
//  $dimensions = array(
//    'width' => $variables['width'],
//    'height' => $variables['height'],
//  );
//
//  image_style_transform_dimensions($variables['style_name'], $dimensions);
//
//  $variables['width'] = $dimensions['width'];
//  $variables['height'] = $dimensions['height'];
//
//  // Determine the URL for the styled image.
//  $variables['path'] = image_style_url($variables['style_name'], $variables['path']);
//  return theme('image', $variables);
//}

function panelitist_image_style($variables) {
  $style_name = $variables['style_name'];
  $path = $variables['path'];
  

// theme_image() can only honor the $getsize parameter with local file paths.
  // The derivative image is not created until it has been requested so the file
  // may not yet exist, in this case we just fallback to the URL.
  $style_path = image_style_path($style_name, $path);
  if (!file_exists($style_path)) {
    $style_path = image_style_url($style_name, $path);
  }
  $variables['path'] = $style_path;
  if (

is_file($style_path)) {
    if (list($width, $height, $type, $attributes) = @getimagesize($style_path)) {
      $variables['width'] = $width;
      $variables['height'] = $height;
    }
  }
  
  return theme('image', $variables);
}
