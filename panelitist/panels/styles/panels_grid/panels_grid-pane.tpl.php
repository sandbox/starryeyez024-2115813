<div class="panel-pane <?php echo $settings['grid_width'] . ' ' . $settings['float'] . ' ' . $settings['clear_floats'];  ?> column">
  <div class="pane-content <?php echo $settings['image_size'] . ' ' . $settings['image_layout']; ?>">
  <?php print render($content->content); ?>
  </div>
</div>
