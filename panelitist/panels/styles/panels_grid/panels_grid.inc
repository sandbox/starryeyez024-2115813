<?php

$plugin = array(
  'panels_grid' => array(
    'title'              => t('Panels Grid'),
    'description'        => t('Thin panel pane and region wrappers with custom classes'),
    'render pane'        => 'panels_grid_render_pane',
    'pane settings form' => 'panels_grid_settings_form',
    'css'                => '../../../css/panels/panels_grid.css',

    'hook theme'         => array(
      'panels_grid_theme_pane' => array(
        'template'  => 'panels_grid-pane',
        //'path'      => drupal_get_path('module', 'twc_thematic') . '/plugins/styles/panels_grid',
        'path'      => drupal_get_path('theme', 'panelitist') . '/panels/styles/panels_grid',
        'variables' => array(
          'content'  => NULL,
          'settings' => NULL,
        ),
      ),
    ),
  ),
);

function theme_panels_grid_render_pane($vars) {
  $content  = $vars['content'];
  $settings = $vars['settings'];
  // This renders the template file panels_grid-pane.tpl.php
  return theme(
    'panels_grid_theme_pane',
    array(
      'content'  => $content,
      'settings' => $settings
    )
  );
}

// Configuration form for the pane/region settings.
function panels_grid_settings_form($style_settings, $panel_obj = null, $pane_id = null, $type = null) {
      $form['grid_width'] = array(
        '#type'          => 'select',
        '#title'         => t('Panel Pane Width'),
        '#options'       => array(
          'width-100' => t('100%  width'),
          'width-75'  => t('75% width'),
          'width-66'  => t('66% width'),
          'width-50'  => t('50% width'),
          'width-33'  => t('33% width'),
          'width-25'  => t('25% width'),
          ' '       => t('-None-'),
        ),
        '#default_value' => (isset($style_settings['grid_width'])) ? $style_settings['grid_width'] : 'width-100',
      );
    $form['image_size'] = array(
    '#type'          => 'select',
    '#title'         => t('Image size override'),
    '#options'       => array(
      ' '           => t('-None-'),
      //'img-xs' => t('Image XS = Image is at least 90px wide'),
      'img-sm' => t('Image SM = Image is at least 125px wide'),
      'img-md' => t('Image MD = Image is at least 320px wide'),
      'img-lg' => t('Image LG = Image is at least 485px wide'),
      'img-xl' => t('Image XL = Image is at least 650px wide'),
      //'img-xxl' => t('Image XXL = Image is at least 815px wide'),
    ),
    '#default_value' => (isset($style_settings['image_size'])) ? $style_settings['image_size'] : 'img-xs',
  );
  $form['image_layout'] = array(
    '#type'          => 'select',
    '#title'         => t('Image Layout'),
    '#options'       => array(
      ' '           => t('-None-'),
      'image-full'  => t('Image is 100% width, no stretching '),
      'image-float' => t('Image floats, text flows around to left'),
    ),
    '#default_value' => (isset($style_settings['image_layout'])) ? $style_settings['image_layout'] : ' ',
  );
     $form['float'] = array(
        '#type'          => 'select',
        '#title'         => t('Float'),
        '#options'       => array(
          ' '       => t('-None-'),
          'float-left'  => t('float left'),
          'float-right'  => t('float right'),
        ),
        '#default_value' => (isset($style_settings['float'])) ? $style_settings['float'] : ' ',
      );
    $form['clear_floats'] = array(
        '#type'          => 'select',
        '#title'         => t('Clear Floats'),
        '#options'       => array(
          ' '       => t('-None-'),
          'clear-left'  => t('clear left'),
          'clear-right'  => t('clear right'),
          'clear-both'  => t('clear both'),
        ),
        '#default_value' => (isset($style_settings['clear_floats'])) ? $style_settings['clear_floats'] : ' ',
      );



  return $form;
}

